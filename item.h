#ifndef ITEM_H
#define ITEM_H

#include <iostream>
#include <queue>
#include <vector>
#include <string>
using namespace std;

class item
{
private:
  /* data */
  string m_name;
  float m_price;
  int m_ID;

public:
  item(/* args */);
  // ~item();

  friend void addItem(vector<item> &list);

  //for vector
  friend void printList(const vector<item> list);
  //overloaded for priority_queue
  friend void printList(priority_queue<item> pq); 

  friend void splitList(const vector<item> list);  //will create two min priority queues

  friend vector<item> removeItem(vector<item> &list);
  
  //helper function
  friend float getTotalPrice(vector<item> list);
  //overloaded for priority_queue
  friend float getTotalPrice(priority_queue<item> pq);

  bool operator< (const item &i1) const;
};




#endif