//Item object functions
#include <iostream>
#include <queue>
#include <vector>
#include <string>
#include <limits>
#include <cmath>
#include "item.h"
using namespace std;


item::item(/* args */)
{
  //default name
  m_name = "No Name";
  //default price
  m_price = 0;
  //add ID to items  
  static int newID = 0;
  newID++;
  m_ID = newID;
}

// item::~item()
// {
// }

void addItem(vector<item> &list)
{
  string nameIn;
  float priceIn;
  item newItem;


  cout << "Enter item name:\n";
  cin.ignore();
  getline(cin, nameIn, '\n');
  newItem.m_name = nameIn;

  cout << "Enter Price\n";
  cin >> priceIn;

  //validates input type
  while(1)
  {
    if(cin.fail())
    {
      cin.clear();
      cin.ignore(numeric_limits<streamsize>::max(),'\n');
      cout << "You have entered wrong input type!" <<endl;
      cin >> priceIn;
    }
    if(!cin.fail())
      break;
  }

  newItem.m_price = priceIn;

  list.push_back(newItem);
}

//helper function
float getTotalPrice(vector<item> list)
{
  float totalPrice = 0.0;
  for(int i = 0; i < list.size(); i++)
  {
    totalPrice += list[i].m_price;
  }
  
  return totalPrice;
}
//overloaded for priority_queue
float getTotalPrice(priority_queue<item> pq)
{
  float totalPrice = 0.0;
  int pqSize = pq.size();

  for(int i = 0; i < pqSize; i++)
  {
    totalPrice += pq.top().m_price;
    pq.pop();
  }
  
  return totalPrice;
}

void printList(const vector<item> list)
{
  for(int i = 0; i < list.size(); i++)
  {
    cout << "ID: " << list[i].m_ID << "\t"<< list[i].m_name << (list[i].m_name.length() < 8? "\t": "")<< "\t$" <<list[i].m_price << endl;
  }

  cout << "You have " << list.size() << " items in the list.\n";
  cout << "Total Price is: $" << getTotalPrice(list) << endl;
}
//overloaded for priority_queue
void printList(priority_queue<item> pq)
{
  float pqPrice = getTotalPrice(pq);
  int pqSize = pq.size();
  

  for(int i = 0; i < pqSize; i++)
  {
    cout << "ID: " << pq.top().m_ID << "\t"
         << pq.top().m_name << (pq.top().m_name.length() < 8? "\t": "") << "\t$" 
         << pq.top().m_price << endl;
    pq.pop();
  }

  cout << "You have " << pqSize << " items in the list.\n";
  cout << "Total Price is: $" << pqPrice << endl;
}

void splitList(const vector<item> list)  //will create two min priority queues
{
  priority_queue<item> pq1;
  priority_queue<item> pq2;
  float tPrice1 = 0.0;
  float tPrice2 = 0.0;
  float lowestItemPrice = 0.0;
  bool evenSplit = false;

  // Reject if list contains < 2 items
  if(list.size() < 2)
  {
    cout << "Error! There must be at least two items in the list." << endl;
    return;
  }

  //finds number of items for each list
  int numItems1 = 0;
  if(list.size()%2 == 0)  //if even number
    numItems1 = list.size()/2;
  else if(list.size()%2 == 1) //if odd number
    numItems1 = (list.size()/2)+1;
  int numItems2 = list.size()/2;

  //place items in priority queues
  for(int i = 0; i < numItems1; i++)
  {
    pq1.emplace(list[i]);
  }
  for(int i = numItems1; i < list.size(); i++)
  {
    pq2.emplace(list[i]);
  }

  //find lowest priced item of the two
  if(pq1.top().m_price < pq2.top().m_price)
  {
    lowestItemPrice = pq1.top().m_price;
  }
  else
  {
    lowestItemPrice = pq2.top().m_price;
  }
  

  tPrice1 = getTotalPrice(pq1);
  tPrice2 = getTotalPrice(pq2);
  float lowestDiff = fabs(tPrice1 - tPrice2); //absolute value

  do
  {
    tPrice1 = getTotalPrice(pq1);
    tPrice2 = getTotalPrice(pq2);
    float priceDiff = fabs(tPrice1 - tPrice2); //absolute value
    
    if(lowestDiff > priceDiff)
    {
      lowestDiff = priceDiff;
    }

    if(tPrice1 == tPrice2 || tPrice1+0.01 == tPrice2 || tPrice1-0.01 == tPrice2 || list.size() <= 2)
    {
      //DONE if equal or off by one cent or <= 2 items in list
      evenSplit = true;
      break;
    }
    else if(tPrice1 > tPrice2)
    {
      //move top of pq1 to pq2
      pq2.push(pq1.top());
      pq1.pop();
    }
    else if(tPrice2 > tPrice1)
    {
      //move top of pq2 to pq1
      pq1.push(pq2.top());
      pq2.pop();
    }

    //absolute value
    float nextPriceDiff = fabs(getTotalPrice(pq1) - getTotalPrice(pq2)); 

    if(lowestDiff > nextPriceDiff)
    {
      lowestDiff = nextPriceDiff;
    }

    if(lowestDiff <= lowestItemPrice)
    {
      evenSplit = true;
      break;
    }
  } while (!evenSplit);

  //print the lists to screen
  cout << "List1: " << endl;
  printList(pq1);

  cout << endl << "List2: " << endl;
  printList(pq2);

  cout << endl << "List Price Difference: " << lowestDiff << endl;

}

vector<item> removeItem(vector<item> &list)
{
  int removeID = 0;
  int removeIndex = -1;
  
  cout << "Enter ID to remove an Item\n";
  cin >> removeID;

  //finds the ID
  for(int i = 0; i < list.size(); i++)
  {
    if(removeID == list[i].m_ID)
    { 
      removeIndex = i;
      break;
    }
  }
  //if valid ID entered
  if(removeIndex != -1)
  {
    cout << "Removed " << list[removeIndex].m_name << endl;
    list.erase(list.begin() + removeIndex);
  }
  else
  {
    //if invalid ID entered
    cout << "ID: " << removeID << " does not exist." << endl;
  }
  

  return list;
}

bool item::operator< (const item &i1) const
{
  return m_price > i1.m_price;  //> for a min heap
}
