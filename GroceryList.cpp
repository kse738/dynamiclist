//Kevin Ehlen

#include <iostream>
#include <queue>
#include <vector>
#include <string>
#include "item.h"
#include "GroceryList.h"
using namespace std;


int main()
{
  vector<item> list;

  string usrChoice = "0";
  bool quitChoice = false;
  bool notValid = true;
  
  //formating
  cout.setf(ios::fixed); 
  cout.setf(ios::showpoint); 
  cout.precision(2); 

  //Loop until "Quit" is selected
  do
  {
    //Prompt user
    cout << "\nPlease select an option from the menu.\n";
    //Menu
    cout << "-----Options-----\n"
         << "1. Add an Item\n"
         << "2. Print the List\n"
         << "3. Split evenly by price\n"
         << "4. Remove an Item\n"
         << "-1. Quit\n\n";
    
    //User Input for menu
    cin >> usrChoice;

    // Handle user choice
    if(usrChoice == "1")
    {
      addItem(list);  //just cin and return item in fuction
    }
    else if(usrChoice == "2")
    {
      printList(list);
    }
    else if(usrChoice == "3")
    {
      splitList(list);
    }
    else if(usrChoice == "4")
    {
      list = removeItem(list); //only removes from main list
    }
    else if(usrChoice == "-1")
    {
      quitChoice = quit();  //needs to return userChoice, if canceled, change to 0
    }
    else
    {
      cout << "Invalid input" << endl;
    }

  } while(!quitChoice);

  return 0;
}